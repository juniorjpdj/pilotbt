package cf.juniorjpdj.pilotbt;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

public class BluetoothControl {

  public BluetoothSocket btSocket = null;
  public BluetoothDevice btDevice;
  
  private final Collection<StatusListener> statusListeners = new ArrayList<StatusListener>(2);
  
  
  public interface StatusListener {
    void onStatusChange(Boolean connected, BluetoothDevice device);
  }
  
  public void registerStatusListener(StatusListener lis){
  statusListeners.add(lis);
  }
  
  public void ChangeStatus(Boolean connected, BluetoothDevice device){
    for(StatusListener lis : statusListeners){
      lis.onStatusChange(connected, device);
    }
  }

  public void Connect(BluetoothDevice device) throws Exception {
	btDevice = device;
    try {
      BluetoothSocket socket = device
          .createRfcommSocketToServiceRecord(UUID
              .fromString("00001101-0000-1000-8000-00805F9B34FB"));
      socket.connect();
      btSocket = socket;
      ChangeStatus(true, device);

    } catch (Exception ex) {
      Disconnect();
      ex.printStackTrace();
      throw ex;
    }

  }

  public void Disconnect() {
    try {
      btSocket.close();
    } catch (Exception e) {}
    btSocket = null;
    ChangeStatus(false, null);
  }

  public void Send(String txt) throws Exception {

    if (btSocket == null) {
      throw new Exception();
    } else {
      try {
        btSocket.getOutputStream().write(txt.getBytes());
      } catch (Exception ex) {
        Disconnect();
        ex.printStackTrace();
        throw ex;
      }
    }
  }

  public String getRespond() throws Exception {
    try {
      InputStream s = btSocket.getInputStream();
      byte[] bytes = new byte[s.available()];
      s.read(bytes);
      return new String(bytes, "UTF-8");
    } catch (Exception ex) {
      ex.printStackTrace();
      throw ex;
    }
  }

}
