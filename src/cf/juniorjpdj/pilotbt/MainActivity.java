package cf.juniorjpdj.pilotbt;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.PorterDuff.Mode;

@SuppressLint("UseSparseArrays")
@SuppressWarnings("serial")
public class MainActivity extends ActionBarActivity implements OnSeekBarChangeListener {

  private static MainActivity act;
  private static TextView info;
  private SharedPreferences prefs;
  private BluetoothControl btCtrl;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    prefs = PreferenceManager.getDefaultSharedPreferences(this);
    setContentView(R.layout.activity_main);
    act = this;
    btCtrl = new BluetoothControl();
    btCtrl.registerStatusListener(new BluetoothControl.StatusListener() {
        @Override
        public void onStatusChange(Boolean connected, BluetoothDevice device){
        	if(connected){
        		setInfo(String.format(act.getString(R.string.connected), device.getName()));
        	} else {
        		setInfo(R.string.not_connected);
        	}
        }
    });
    info = (TextView) findViewById(R.id.info);

    SeekBar slider = (SeekBar) findViewById(R.id.slider);
    SeekBar slider1 = (SeekBar) findViewById(R.id.slider1);
    SeekBar slider2 = (SeekBar) findViewById(R.id.slider2);
    slider.setOnSeekBarChangeListener(this);
    slider1.setOnSeekBarChangeListener(this);
    slider2.setOnSeekBarChangeListener(this);
  }

  @Override
  protected void onResume() {
    super.onResume();

    final HashMap<Integer, String> ranges = new HashMap<Integer, String>() {{
        put(R.id.slider, "rangeSlider");
        put(R.id.slider1, "rangeSlider1");
        put(R.id.slider2, "rangeSlider2");
      }
    };
    
    final HashMap<Integer, String> visible = new HashMap<Integer, String>() {{
        put(R.id.button1, "visibleButton1");
        put(R.id.button2, "visibleButton2");
        put(R.id.button3, "visibleButton3");
        put(R.id.button4, "visibleButton4");
        put(R.id.button5, "visibleButton5");
        put(R.id.button6, "visibleButton6");
        put(R.id.button7, "visibleButton7");
        put(R.id.button8, "visibleButton8");
        put(R.id.button9, "visibleButton9");
        put(R.id.slider, "visibleSlider");
        put(R.id.slider1, "visibleSlider1");
        put(R.id.slider2, "visibleSlider2");
      }
    };
    final HashMap<Integer, String> names = new HashMap<Integer, String>() {{
        put(R.id.button1, "nameButton1");
        put(R.id.button2, "nameButton2");
        put(R.id.button3, "nameButton3");
        put(R.id.button4, "nameButton4");
        put(R.id.button5, "nameButton5");
        put(R.id.button6, "nameButton6");
        put(R.id.button7, "nameButton7");
        put(R.id.button8, "nameButton8");
        put(R.id.button9, "nameButton9");
      }
    };
    final HashMap<Integer, String> colors = new HashMap<Integer, String>() {{
        put(R.id.button1, "colButton1");
        put(R.id.button2, "colButton2");
        put(R.id.button3, "colButton3");
        put(R.id.button4, "colButton4");
        put(R.id.button5, "colButton5");
        put(R.id.button6, "colButton6");
        put(R.id.button7, "colButton7");
        put(R.id.button8, "colButton8");
        put(R.id.button9, "colButton9");
      }
    };

    for (Integer i : visible.keySet()) {
      if (prefs.getBoolean(visible.get(i), true)) {
        findViewById(i).setVisibility(0);
      } else {
        findViewById(i).setVisibility(4);
      }
    }

    for (Integer i : names.keySet()) {
      String name = prefs.getString(names.get(i), "");
      if (!name.equals("")) {
        ((Button) findViewById(i)).setText(name);
      }
    }

    for (Integer i : colors.keySet()) {
      Integer color = Integer
          .parseInt(prefs.getString(colors.get(i), "1"));
      if (color != 1) {
        ((Button) findViewById(i)).getBackground().setColorFilter(
            color, Mode.MULTIPLY);
      } else {
        ((Button) findViewById(i)).getBackground().clearColorFilter();
      }
    }

    for (Integer i : ranges.keySet()) {
      Integer max = Integer
          .parseInt(prefs.getString(ranges.get(i), "100"));
      ((SeekBar) findViewById(i)).setMax(max);
    }
    
    if (btCtrl.btSocket != null) {
      setInfo(String.format(getString(R.string.connected), btCtrl.btDevice.getName()));
    }
  }

  @Override
  public void onStopTrackingTouch(SeekBar slider){
    findViewById(R.id.sliderVal).setVisibility(4);
    findViewById(R.id.arrows).setVisibility(0);
    String sliderName = "";
    switch (slider.getId()) {
    case R.id.slider:
      sliderName = "D";
      break;
    case R.id.slider1:
      sliderName = "L";
      break;
    case R.id.slider2:
      sliderName = "R";
      break;
    }
    if (!sliderName.equals("")) {
      Send(sliderName + (char) slider.getProgress());
    }
  }

  @Override
  public void onProgressChanged(SeekBar slider, int b, boolean c) {
    TextView sliderVal = (TextView) findViewById(R.id.sliderVal);
    sliderVal.setText(Integer.valueOf(b).toString());
  }

  @Override
  public void onStartTrackingTouch(SeekBar slider) {
    findViewById(R.id.arrows).setVisibility(4);
    findViewById(R.id.sliderVal).setVisibility(0);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    switch (id) {
    case R.id.connect:
      Connect();
      return true;
    case R.id.disconnect:
      btCtrl.Disconnect();
      return true;
    case R.id.settings:
      Intent ustawienia = new Intent(this, SettingsActivity.class);
      startActivity(ustawienia);
      return true;
    }
    return super.onOptionsItemSelected(item);
  }

  public void onClick(View v) {
    // String send = "";
    final HashMap<Integer, String> buttons = new HashMap<Integer, String>() {
      /**
     * 
     */
      private static final long serialVersionUID = 6449996060476676350L;

      {
        put(R.id.up, "u");
        put(R.id.down, "d");
        put(R.id.center, "c");
        put(R.id.left, "l");
        put(R.id.right, "r");
        put(R.id.button1, "A");
        put(R.id.button2, "B");
        put(R.id.button3, "C");
        put(R.id.button4, "D");
        put(R.id.button5, "E");
        put(R.id.button6, "F");
        put(R.id.button7, "G");
        put(R.id.button8, "H");
        put(R.id.button9, "I");
      }
    };

    if (buttons.containsKey(v.getId())) {
      Send(buttons.get(v.getId()));
    }
  }

  public static MainActivity getActivity() {
    return act;
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    switch (requestCode) {
    case 9:
      if (resultCode == Activity.RESULT_OK) {
        Connect();
      } else {
        sendError(getString(R.string.user_foch));
      }
      break;

    default:
      super.onActivityResult(requestCode, resultCode, data);
      break;
    }
  }

  public void Send(String s){
	  try {
		  btCtrl.Send(s);
		  sendInfo(String.format(getString(R.string.sent), s));
	  } catch(Exception ex) {
		  sendError(String.format(getString(R.string.sending_error), s, ex.toString()));
	  }
  }
  
	public void Connect() {
		btCtrl.Disconnect();
		BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
		if (adapter != null) {
			if (!adapter.isEnabled()) {
				Intent i = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
				act.startActivityForResult(i, 9);
			} else {
				String[] devN = {};
				final List<BluetoothDevice> dev = new ArrayList<BluetoothDevice>();
				for (BluetoothDevice device : adapter.getBondedDevices()) {
					dev.add(device);
					final int N = devN.length;
					devN = Arrays.copyOf(devN, N + 1);
					devN[N] = device.getName();
				}

				AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(act);
				dialogBuilder.setTitle(R.string.devices);
				dialogBuilder.setItems(devN,
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,	int which) {
								try {
									btCtrl.Connect(dev.get(which));
								} catch(Exception ex) {
									sendError(String.format(act.getString(R.string.connecting_error), ex.toString()));
								}
							}
						});
				Dialog dialog = dialogBuilder.create();
				dialog.show();
			}
		} else sendError(getString(R.string.no_bt));
	}
  
  public void setInfo(String s) {
    info.setText(s);
  }
  
  public void setInfo(int i){
    info.setText(i);
  }
  
  public static void sendInfo(String s) {
    final Toast t = Toast.makeText(act, s, Toast.LENGTH_LONG);
    t.show();
    Handler h = new Handler();
    h.postDelayed(new Runnable() {
      @Override
      public void run() {
        t.cancel();
      }
    }, 500);
  }

  public static void sendError(String s) {
    Toast.makeText(act, s, Toast.LENGTH_LONG).show();
  }
}
