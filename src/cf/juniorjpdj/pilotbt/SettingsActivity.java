package cf.juniorjpdj.pilotbt;

import java.util.HashSet;

import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.MultiSelectListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceGroup;

@SuppressWarnings("deprecation")
public class SettingsActivity extends PreferenceActivity implements OnSharedPreferenceChangeListener{
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    addPreferencesFromResource(R.xml.settings);
    initSummary(getPreferenceScreen());

  }

  @Override
  protected void onResume() {
    super.onResume();
    getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
  }

  @Override
  public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
	Preference p = findPreference(key);
    updateSummary(p);
  }

  public void initSummary(Preference p) {
    if (p instanceof PreferenceGroup) {
      PreferenceGroup pGrp = (PreferenceGroup) p;
      for (int i = 0; i < pGrp.getPreferenceCount(); i++) {
        initSummary(pGrp.getPreference(i));
      }
    } else {
      updateSummary(p);
    }
  }

  public void updateSummary(Preference p) {
    final HashSet<String> keys = new HashSet<String>(){
	  private static final long serialVersionUID = -8237563504550060365L;{

      add("nameButton1");
      add("nameButton2");
      add("nameButton3");
      add("nameButton4");
      add("nameButton5");
      add("nameButton6");
      add("nameButton7");
      add("nameButton8");
      add("nameButton9");
      add("colButton1");
      add("colButton2");
      add("colButton3");
      add("colButton4");
      add("colButton5");
      add("colButton6");
      add("colButton7");
      add("colButton8");
      add("colButton9");
      add("rangeSlider");
      add("rangeSlider1");
      add("rangeSlider2");
    }};

    if(keys.contains(p.getKey())){
      if (p instanceof ListPreference) {
        ListPreference listPref = (ListPreference) p;
        p.setSummary(listPref.getEntry());
      } else if (p instanceof EditTextPreference) {
        EditTextPreference editTextPref = (EditTextPreference) p;
        p.setSummary(editTextPref.getText());
      } else if (p instanceof MultiSelectListPreference) {
        EditTextPreference editTextPref = (EditTextPreference) p;
        p.setSummary(editTextPref.getText());
      }
    }
  }
}